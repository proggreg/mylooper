//
//  Looper.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "Looper.h"

Looper::Looper()
{
    //initialise - not playing / recording
    playState = false;
    recordState = false;
    //position to the start of audioSampleBuffer
    bufferPosition = 0;
    
    
    
    //audioSampleBuffer contents to zero
    for (int count = 0; count < bufferSize; count++)
        
//      audioSampleBuffer[count] = 0.f;
        
        audiosamplebuffer.setSize(1,bufferSize);
        audiosamplebuffer.clear();

}

Looper::~Looper()
{

}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState.get();
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState.get();
}

float Looper::processSample (float input)
{
    float* getWritePointer (int channelNumber, int sampleOffset);
    float* audioSample;
    float output;
    
    if (playState.get() == true)
    {
        //play
//        output = audioSampleBuffer[bufferPosition];
        audioSample = audiosamplebuffer.getWritePointer(0,bufferPosition);
        output = *audioSample;
//        output = audioSampleBuffer.getWritePointer(0, bufferPosition);
        
        
        //click 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 4)) == 0)
            output += 0.25f;
        
        //record
        if (recordState.get() == true)
//            audioSampleBuffer[bufferPosition] = input;
//            *getWritePointer(0, bufferPosition) = input;
        *audioSample += input;
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
        
        
    }
    return output;
}
